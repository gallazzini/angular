import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../services/connection.service';
import { Student } from '../objects/student';
import { AcceptForm } from '../objects/accept-form';
import { DataCacheService } from '../services/data-cache.service';

@Component({
  selector: 'app-accept-rgpd',
  templateUrl: './accept-rgpd.component.html',
  styleUrls: ['./accept-rgpd.component.css']
})
export class AcceptRgpdComponent implements OnInit {
  form: AcceptForm;

  constructor(private connectionService: ConnectionService, private dataService: DataCacheService) {
    this.form = {
      accept: (connectionService.loggedUser as Student).rgpdaccepted
    };
   }

  ngOnInit() {
  }

  onChanged() {
    (this.connectionService.loggedUser as Student).rgpdaccepted = this.form.accept;
    (this.dataService.getUser(this.connectionService.loggedUser.id) as Student).rgpdaccepted = this.form.accept;
    this.dataService.save();
  }
}
