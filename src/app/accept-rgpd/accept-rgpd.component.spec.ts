import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AcceptRgpdComponent } from './accept-rgpd.component';

describe('AcceptRgpdComponent', () => {
  let component: AcceptRgpdComponent;
  let fixture: ComponentFixture<AcceptRgpdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AcceptRgpdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptRgpdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
