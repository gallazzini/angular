import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../services/connection.service';
import { UserType } from '../objects/user-type';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private connectionService: ConnectionService) { }

  ngOnInit() {
  }

  isStudent() {
    return this.connectionService.isUserType(UserType.Student);
  }
}
