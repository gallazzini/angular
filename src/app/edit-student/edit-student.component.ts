import { Component, OnInit } from '@angular/core';
import { Student } from '../objects/student';
import { DataCacheService } from '../services/data-cache.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-student',
  templateUrl: './edit-student.component.html',
  styleUrls: ['./edit-student.component.css']
})
export class EditStudentComponent implements OnInit {
  student: Student;

  constructor(private dataService: DataCacheService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.params.subscribe(
      p => {
        this.student = this.dataService.getUser(p['id']) as Student;
      }
    )
  }
  
  edit() {
    this.dataService.updateStudent(this.student.id, this.student);
    this.router.navigate(['/students']);
  }
}
