import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { AcceptRgpdComponent } from './accept-rgpd/accept-rgpd.component';
import { RgpdComponent } from './rgpd/rgpd.component';
import { StudentsComponent } from './students/students.component';
import { StatsComponent } from './stats/stats.component';
import { HomeComponent } from './home/home.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { EditStudentComponent } from './edit-student/edit-student.component';
import { Error404Component } from './error404/error404.component';
import { HttpClientModule } from '@angular/common/http';
import { DataCacheService } from './services/data-cache.service';
import { ConnectionService } from './services/connection.service';
import { StudentsOnlyPipe } from './pipes/students-only.pipe';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { LogoutComponent } from './logout/logout.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    AcceptRgpdComponent,
    RgpdComponent,
    StudentsComponent,
    StatsComponent,
    HomeComponent,
    ViewStudentComponent,
    EditStudentComponent,
    Error404Component,
    StudentsOnlyPipe,
    LoginComponent,
    LogoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [DataCacheService, ConnectionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
