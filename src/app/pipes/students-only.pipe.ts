import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../objects/user';
import { Student } from '../objects/student';
import { UserType } from '../objects/user-type';

@Pipe({
  name: 'studentsOnly'
})
export class StudentsOnlyPipe implements PipeTransform {

  transform(users: User[]): Student[] {
    return users.filter(user => user.type === UserType.Student) as Student[];
  }

}
