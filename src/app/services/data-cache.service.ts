import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../objects/user';
import { Student } from '../objects/student';

@Injectable({
  providedIn: 'root'
})
export class DataCacheService {
  users: Array<User> = [];

  constructor(private http: HttpClient) {
    if (!this.loadFromLocalStorage()) {
    this.loadFromJSON();
    this.save();
    }
  }

  loadFromLocalStorage() {
    const usersStr = sessionStorage.getItem('users');
    if (usersStr) {
      this.users = JSON.parse(usersStr);
    }
    return this.users.length > 0;
  }

  loadFromJSON() {
    this.http.get<Array<User>>('assets/data/users.json').subscribe(
      loadedUsers => this.users = loadedUsers
    );
  }

  save() {
    sessionStorage.setItem('users', JSON.stringify(this.users));
  }

  getUser(userId: string) {
    return this.users.find(obj => obj.id === userId);
  }

  deleteUser(userId: string) {
    this.users = this.users.filter(user => user.id !== userId);
    this.save();
  }

  updateStudent(id: string, student: Student) {
    const index = this.users.findIndex(user => user.id === id);
    if (index !== -1) {
      this.users[index] = student;
    }
    this.save();
  }
}
