import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ConnectionService } from '../connection.service';
import { UserType } from 'src/app/objects/user-type';

@Injectable({
  providedIn: 'root'
})
export class IsStudentGuard implements CanActivate {
  constructor(private connectionService: ConnectionService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.connectionService.isUserType(UserType.Student);
  }

}
