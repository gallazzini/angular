import { TestBed, async, inject } from '@angular/core/testing';

import { CanViewUserGuard } from './can-view-user.guard';

describe('CanViewUserGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CanViewUserGuard]
    });
  });

  it('should ...', inject([CanViewUserGuard], (guard: CanViewUserGuard) => {
    expect(guard).toBeTruthy();
  }));
});
