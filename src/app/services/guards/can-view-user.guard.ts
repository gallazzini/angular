import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ConnectionService } from '../connection.service';
import { UserType } from 'src/app/objects/user-type';
import { DataCacheService } from '../data-cache.service';
import { OptionDirector } from 'src/app/objects/option-director';
import { Student } from 'src/app/objects/student';

@Injectable({
  providedIn: 'root'
})
export class CanViewUserGuard implements CanActivate {

  constructor(private connectionService: ConnectionService, private dataService: DataCacheService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.connectionService.isLoggedIn() && (
      this.connectionService.isUserType(UserType.Admin) ||
      (this.connectionService.isUserType(UserType.Student) && this.connectionService.loggedUser.id === route.params.id) ||
      (this.connectionService.isUserType(UserType.OptionDirector) &&
       (this.connectionService.loggedUser as OptionDirector).option === (this.dataService.getUser(route.params.id) as Student).option));
  }

}
