import { Injectable } from '@angular/core';
import { DataCacheService } from './data-cache.service';
import { User } from '../objects/user';
import { UserType } from '../objects/user-type';

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {
  loggedUser: User;

  constructor(private dataService: DataCacheService) {
    const loggedUserStr = sessionStorage.getItem('loggedUser');
    if(loggedUserStr) {
      this.loggedUser = JSON.parse(loggedUserStr);
    }
  }

  login(user: string, password: string) {
    this.loggedUser = this.dataService.users.find(obj => obj.user === user && obj.password === password);

    if (this.loggedUser) {
      sessionStorage.setItem('loggedUser', JSON.stringify(this.loggedUser));
    }

    return !!this.loggedUser;
  }

  isLoggedIn() {
    return !!this.loggedUser;
  }

  isUserType(userType: UserType) {
    return this.loggedUser && this.loggedUser.type === userType;
  }
}
