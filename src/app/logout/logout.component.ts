import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../services/connection.service';
import { NgLocalization } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private connectionService: ConnectionService, private router: Router) {
    connectionService.loggedUser = null;
    this.router.navigate(['/']);
   }

  ngOnInit() {
  }

}
