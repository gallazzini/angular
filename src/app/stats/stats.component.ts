import { Component, OnInit } from '@angular/core';
import * as Chart from 'chart.js';
import { DataCacheService } from '../services/data-cache.service';
import { UserType } from '../objects/user-type';
import { Student } from '../objects/student';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  ctx: object;

  constructor(private dataService: DataCacheService) { }

  ngOnInit() {
    this.ctx = document.getElementById('myChart');
  }

  studentsByYear() {
    let data = this.dataService.users
      .filter(user => user.type === UserType.Student)
      .map(user => (user as Student).graduateyear)
      .reduce(
        (sum, current) => {
          sum[current] = (current in sum ? sum[current] : 0) + 1;
          return sum;
        },
        {});

    let chart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: Object.keys(data),
        datasets: [{
          label: 'Number of graduates by year',
          data: Object.values(data),
        }]
      },
      options: {
        responsive: false,
        display: true
      }
    });
  }

  salaryByCity() {
    let data = this.dataService.users
      .filter(user => user.type === UserType.Student)
      .reduce(
        (sum, current) => {
          let s = current as Student;
          if (!(s.city in sum)) {
            sum[s.city] = {nb: 0, tot: 0};
          }
          sum[s.city].nb++;
          sum[s.city].tot += s.salary;
          return sum;
        },
        {});

    // tslint:disable-next-line: forin
    for (const i in Object.keys(data)) {
      let city = Object.keys(data)[i];
      data[city] = data[city].tot / data[city].nb;
    }
    
    let chart = new Chart(this.ctx, {
      type: 'bar',
      data: {
        labels: Object.keys(data),
        datasets: [{
          label: 'Salary by cities',
          data: Object.values(data),
        }]
      },
      options: {
        responsive: false,
        display: true
      }
    });
  }
}
