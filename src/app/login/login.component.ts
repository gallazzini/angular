import { Component, OnInit } from '@angular/core';
import { ConnectionService } from '../services/connection.service';
import { LoginForm } from '../objects/login-form';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  form: LoginForm;
  message: string;

  constructor(private connectionService: ConnectionService) { }

  ngOnInit() {
    this.form = {
      user: '',
      password: ''
    };
  }

  login() {
    if(this.connectionService.login(this.form.user, this.form.password)) {
      this.message = 'Successfuly connected !';
    } else {
      this.message = 'User or password incorrect.';
    }
  }
}
