import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RgpdComponent } from './rgpd/rgpd.component';
import { AcceptRgpdComponent } from './accept-rgpd/accept-rgpd.component';
import { StudentsComponent } from './students/students.component';
import { StatsComponent } from './stats/stats.component';
import { EditStudentComponent } from './edit-student/edit-student.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { HomeComponent } from './home/home.component';
import { Error404Component } from './error404/error404.component';
import { LoginComponent } from './login/login.component';
import { CanViewUserGuard } from './services/guards/can-view-user.guard';
import { IsAdminGuard } from './services/guards/is-admin.guard';
import { IsStudentGuard } from './services/guards/is-student.guard';
import { LogoutComponent } from './logout/logout.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'rgpd', component: RgpdComponent},
  {path: 'accept-rgpd', component: AcceptRgpdComponent, canActivate: [IsStudentGuard]},
  {path: 'students', component: StudentsComponent},
  {path: 'stats', component: StatsComponent},
  {path: 'student/:id/edit', component: EditStudentComponent, canActivate: [IsAdminGuard]},
  {path: 'student/:id', component: ViewStudentComponent, canActivate: [CanViewUserGuard]},
  {path: '**', component: Error404Component},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
