import { Component, OnInit } from '@angular/core';
import { DataCacheService } from '../services/data-cache.service';
import { ConnectionService } from '../services/connection.service';
import { UserType } from '../objects/user-type';
import { OptionDirector } from '../objects/option-director';
import { User } from '../objects/user';
import { Student } from '../objects/student';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {
  constructor(private dataService: DataCacheService, private connectionService: ConnectionService) {
   }

  ngOnInit() {
  }

  isUserAdmin() {
    return this.connectionService.isUserType(UserType.Admin);
  }

  canViewUser(student: Student) {
    return this.isUserAdmin() ||
    (this.connectionService.isUserType(UserType.OptionDirector) 
    && (this.connectionService.loggedUser as OptionDirector).option === student.option) ||
    (this.connectionService.isUserType(UserType.Student) && this.connectionService.loggedUser.id === student.id);
  }

  deleteStudent(id: string) {
    this.dataService.deleteUser(id);
  }

  getName(student: Student) {
    return student.rgpdaccepted ? student.name : '*anon*';
  }
}
