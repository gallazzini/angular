import { Component, OnInit } from '@angular/core';
import { DataCacheService } from '../services/data-cache.service';
import { ActivatedRoute } from '@angular/router';
import { Student } from '../objects/student';

@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.css']
})
export class ViewStudentComponent implements OnInit {
  student: Student;

  constructor(private dataService: DataCacheService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(
      p => {
        this.student = this.dataService.getUser(p['id']) as Student;
      }
    )
  }
  
  getName(student: Student) {
    return student.rgpdaccepted ? student.name : '*anon*';
  }
}
