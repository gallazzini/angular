import { UserType } from './user-type';

export class User {
  public id: string;
  public user: string;
  public password: string;
  public type: UserType;
}