import { User } from './user';

export class Student extends User {
  public name: string;
  public age: number;
  public graduateyear: number;
  public salary: number;
  public rgpdaccepted: boolean;
  public company: string;
  public option: string;
  public city: string;
}