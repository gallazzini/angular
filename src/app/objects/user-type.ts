export enum UserType {
  Student = 'student',
  OptionDirector = 'option-director',
  Admin = 'admin'
}