import { User } from './user';

export class OptionDirector extends User {
  public option: string;
}